#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define pi 3.141592653589793

//uppgift 4.2
//Mattias Lindgren

int main()
{
	for (int i = 1; i<=12; i++)
	{
		printf("%d %.0f %.0f\n",i, pow(i,2.0),pow(i,3.0));
	};
	
	return 0;
}